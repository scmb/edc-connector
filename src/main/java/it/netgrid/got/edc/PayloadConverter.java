package it.netgrid.got.edc;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.eurotech.cloud.message.EdcPayload;

public class PayloadConverter {

	private HashMap<String, Object> map;
	private EdcPayload payload;
	private Date capturedOn;

	public PayloadConverter() {
	}

	public EdcPayload convert(Map<String, Object> map) {
		payload = new EdcPayload();
		capturedOn = new Date();
		payload.setTimestamp(capturedOn);

		for (String key : map.keySet()) {
			payload.addMetric(key, map.get(key));
		}

		return payload;
	}

	public Map<String, Object> convert(EdcPayload payload) {
		map = new HashMap<String, Object>();
		map.putAll(payload.metrics());
		return map;
	}
}
