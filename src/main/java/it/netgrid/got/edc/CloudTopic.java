package it.netgrid.got.edc;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eurotech.cloud.client.EdcCallbackHandler;
import com.eurotech.cloud.message.EdcInvalidMessageException;
import com.eurotech.cloud.message.EdcPayload;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import it.netgrid.bauer.Topic;
import it.netgrid.got.edc.properties.CloudPropertiesConfiguration;
import it.netgrid.got.model.MessageFromCloud;
import it.netgrid.got.model.MessageToCloud;

/**
 * La classe ECTopic implementa il callback handler che gestisce le interazioni
 * col Cloud MQTT
 * 
 * @author Alberto Harka
 */
@Singleton
public class CloudTopic implements EdcCallbackHandler {

	private CloudClient cloudClient;
	private PayloadConverter converter;
	private static Map<String, Topic<MessageFromCloud>> topicMap;
	private Logger log;

	@Inject
	public CloudTopic(PayloadConverter converter) {
		this.converter = converter;
		topicMap = new HashMap<String, Topic<MessageFromCloud>>();
		log = LoggerFactory.getLogger(CloudTopic.class);
	}

	@Override
	public void connectionLost() {
		log.warn("###EDC-CONNECTOR:CLOUD: Lost connection to " + cloudClient.getBrokerUrl());
	}

	@Override
	public void connectionRestored() {
		log.warn("###EDC-CONNECTOR:CLOUD: Reconnceted back to " + cloudClient.getBrokerUrl());

	}

	@Override
	public void controlArrived(String assett, String topic, EdcPayload payload, int arg3, boolean arg4) {
		sendToBauer(topic, payload);
	}

	@Override
	public void controlArrived(String arg0, String topic, byte[] payload, int arg3, boolean arg4) {
		sendToBauer(topic, payload);
	}

	@Override
	public void publishArrived(String assett, String topic, EdcPayload payload, int arg3, boolean arg4) {
		sendToBauer(topic, payload);
	}

	@Override
	public void publishArrived(String arg0, String topic, byte[] payload, int arg3, boolean arg4) {
		sendToBauer(topic, payload);
	}

	@Override
	public void published(int arg0) {
		log.info("###EDC-CONNECTOR:CLOUD: publisher message id:" + arg0);
	}

	@Override
	public void subscribed(int arg0) {
		log.info("###EDC-CONNECTOR:CLOUD: SUBSCRIBED || " + arg0);
	}

	@Override
	public void unsubscribed(int arg0) {
		log.info("###EDC-CONNECTOR:CLOUD: UNSUBSCRIBED || " + arg0);
	}

	public void addTopic(String topic, Topic<MessageFromCloud> bauerTopic) {
		topicMap.put(topic, bauerTopic);
	}

	private Topic<MessageFromCloud> getBauerTopic(String topic) {
		if(topicMap.containsKey(topic)){
			return topicMap.get(topic);
		}
		else{
			return null;
		}
	}

	public boolean publish(String topic, MessageToCloud event) {
		return cloudClient.publish(topic, converter.convert(event.getContent()));
	}

	public void setClient(CloudClient cloudClient) {
		this.cloudClient = cloudClient;
	}

	private void sendToBauer(String topic, EdcPayload payload) {
		getBauerTopic(topic).post(new MessageFromCloud(CloudPropertiesConfiguration.HANDLER_NAME, topic, converter.convert(payload)));
	}

	private void sendToBauer(String topic, byte[] bytePayload) {
		EdcPayload payload;
		try {
			payload = EdcPayload.buildFromByteArray(bytePayload);
			getBauerTopic(topic).post(new MessageFromCloud(CloudPropertiesConfiguration.HANDLER_NAME, topic, converter.convert(payload)));
		} catch (EdcInvalidMessageException e) {
			log.error(e.getMessage());
		} catch (IOException e) {
			log.error(e.getMessage());
		} catch (NullPointerException e){
			log.error("Send topic not initialized for: " + topic);
		}
	}

}
