package it.netgrid.got.edc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Guice;
import com.google.inject.Injector;

import it.netgrid.got.edc.inject.modules.TopicModule;
import it.netgrid.got.edc.inject.modules.EdcConnectorModule;
import it.netgrid.got.edc.properties.EdcPropertiesConfiguration;

public class Main {
	static final int POOL_SIZE = 100;

	public static void main(String[] args) {
		Injector injector = Guice.createInjector(new EdcConnectorModule());
		EdcPropertiesConfiguration conf = injector.getInstance(EdcPropertiesConfiguration.class);
		CloudClient cloudClient = injector.getInstance(CloudClient.class);
		CloudTopic ecTopic = injector.getInstance(CloudTopic.class);
		ExecutorService executor = Executors.newFixedThreadPool(POOL_SIZE);
		Logger log = LoggerFactory.getLogger(Main.class);

		if (cloudClient.connect()) {
			log.info("###EDC-CONNECTOR: Connected to: " + cloudClient.getBrokerUrl() + "\n");
		} else {
			log.info("###EDC-CONNECTOR: Unable to connect to: " + cloudClient.getBrokerUrl() + "\n");
		}

		for (String topic : conf.getTopics()) {
			if (cloudClient.subscribeTo(topic)) {
				log.info("###EDC-CONNECTOR: subscribed to " + topic + "\n");
			} else {
				log.warn("###EDC-CONNECTOR: unable to subscribe to " + topic + "\n");
			}
			Injector child = injector.createChildInjector(
					new TopicModule(topic, conf.getTopicTx(topic), conf.getTopicRx(topic)));

			TopicThread topicThread = child.getInstance(TopicThread.class);
			ecTopic.addTopic(topic, topicThread.getSendTopic());
			executor.execute(topicThread);
		}

		boolean run = true;
		while (run) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				run = false;
			}
		}
	}
}
