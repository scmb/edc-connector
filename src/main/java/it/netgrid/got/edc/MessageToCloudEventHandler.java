package it.netgrid.got.edc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.netgrid.bauer.EventHandler;
import it.netgrid.bauer.Topic;
import it.netgrid.got.model.CloudError;
import it.netgrid.got.model.MessageToCloud;

public class MessageToCloudEventHandler implements EventHandler<MessageToCloud> {
	private String topicName;
	private String receiveTopicName;
	private CloudTopic cloudTopic;
	private Topic<CloudError> errorTopic;
	private static final Logger log=LoggerFactory.getLogger(MessageToCloudEventHandler.class);
	MessageToCloudEventHandler(String topicName, String receiveTopicName, CloudTopic cloudTopic, Topic<CloudError> errorTopic){
		this.topicName=topicName;
		this.cloudTopic=cloudTopic;
		this.receiveTopicName=receiveTopicName;
		this.errorTopic=errorTopic;
	}
	
	
	@Override
	public Class<MessageToCloud> getEventClass() {
		return MessageToCloud.class;
	}

	@Override
	public String getName() {
		return receiveTopicName;
	}

	@Override
	public boolean handle(MessageToCloud event) {
		log.debug(String.format("event: %s sender: %s",event.getContent(),event.getSender()));
		if (cloudTopic.publish(topicName, event)) {
			return true;
		} else {
			errorTopic.post(new CloudError(topicName, "publish failed"));
		}

		return true;
	}
}