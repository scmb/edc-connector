package it.netgrid.got.edc.inject.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import it.netgrid.got.edc.CloudClient;
import it.netgrid.got.edc.CloudTopic;
import it.netgrid.got.edc.PayloadConverter;
import it.netgrid.got.edc.properties.CloudPropertiesConfiguration;
import it.netgrid.got.edc.properties.CloudPropertiesConfigurationImplementation;
import it.netgrid.got.edc.properties.EdcPropertiesConfiguration;
import it.netgrid.got.edc.properties.EdcPropertiesConfigurationImplementation;

public class EdcConnectorModule extends AbstractModule {
	private CloudTopic ecTopic;
	private CloudClient cloudClient;

	@Override
	protected void configure() {
		ecTopic = new CloudTopic(buildConverter());
		cloudClient = new CloudClient(buildCloudConfiguration(), ecTopic);
		ecTopic.setClient(cloudClient);
	}

	@Singleton
	@Provides
	public CloudClient buildClient() {
		return cloudClient;
	}

	@Singleton
	@Provides
	public EdcPropertiesConfiguration buildConfiguration() {
		return new EdcPropertiesConfigurationImplementation();
	}

	public CloudPropertiesConfiguration buildCloudConfiguration() {
		return new CloudPropertiesConfigurationImplementation();
	}

	@Singleton
	@Provides
	public PayloadConverter buildConverter() {
		return new PayloadConverter();
	}

	@Singleton
	@Provides
	public CloudTopic buildECTopic() {
		return ecTopic;
	}

}
