package it.netgrid.got.edc.inject.modules;


import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import it.netgrid.bauer.Topic;
import it.netgrid.bauer.TopicFactory;
import it.netgrid.got.edc.properties.EdcPropertiesConfiguration;
import it.netgrid.got.model.CloudError;
import it.netgrid.got.model.MessageFromCloud;
import it.netgrid.got.model.MessageToCloud;
import it.netgrid.got.edc.CloudTopic;
import it.netgrid.got.edc.TopicThread;

public class TopicModule extends AbstractModule {
	private String topic;
	private String sendTopic;
	private String receiveTopic;

	public TopicModule(String topic, String sendTopic, String receiveTopic) {
		this.topic = topic;
		this.sendTopic = sendTopic;
		this.receiveTopic = receiveTopic;
	}

	@Override
	protected void configure() {
	}

	@Provides
	public String getTopic() {
		return topic;
	}

	@Provides
	public TopicThread buildThread(String topic, CloudTopic ecTopic, Topic<MessageFromCloud> sendTopic,
			Topic<MessageToCloud> receiveTopic, Topic<CloudError> errorTopic) {
		return new TopicThread(topic, sendTopic, receiveTopic, errorTopic, ecTopic);
	}

	@Provides
	public Topic<MessageFromCloud> getSendTopic() {
		return TopicFactory.getTopic(sendTopic);
	}

	@Provides
	public Topic<MessageToCloud> getReceiveTopic() {
		return TopicFactory.getTopic(receiveTopic);
	}

	@Singleton
	@Provides
	public Topic<CloudError> getErrorTopic(EdcPropertiesConfiguration conf) {
		return TopicFactory.getTopic(conf.getTopicError());
	}
}
