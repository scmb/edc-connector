package it.netgrid.got.edc;

import javax.inject.Inject;

import it.netgrid.bauer.Topic;
import it.netgrid.got.model.CloudError;
import it.netgrid.got.model.MessageFromCloud;
import it.netgrid.got.model.MessageToCloud;
public class TopicThread implements Runnable {
	private CloudTopic cloudTopic;
	private Topic<MessageFromCloud> sendTopic;
	private Topic<MessageToCloud> receiveTopic;
	private Topic<CloudError> errorTopic;
	private String topicName;

	@Inject
	public TopicThread(String topicName, Topic<MessageFromCloud> sendTopic, Topic<MessageToCloud> receiveTopic,
			Topic<CloudError> errorTopic, CloudTopic cloudTopic) {
		this.sendTopic = sendTopic;
		this.receiveTopic = receiveTopic;
		this.errorTopic = errorTopic;
		this.topicName = topicName;
		this.cloudTopic = cloudTopic;
	}

	@Override
	public void run() {
		if(receiveTopic.getName()!=null){
			MessageToCloudEventHandler handler=new  MessageToCloudEventHandler(topicName, receiveTopic.getName(), cloudTopic, errorTopic);
			receiveTopic.addHandler(handler);
		}
	}

	public Topic<MessageFromCloud> getSendTopic() {
		return sendTopic;
	}

}
