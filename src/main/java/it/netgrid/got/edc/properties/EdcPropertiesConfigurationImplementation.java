package it.netgrid.got.edc.properties;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import it.netgrid.got.utils.PropertiesConfigurationTemplate;


public class EdcPropertiesConfigurationImplementation extends PropertiesConfigurationTemplate
		implements EdcPropertiesConfiguration {

	private static final String DEFAULT_CONFIG_PROPERTIES_PATH =System.getProperty("user.dir")
			+ "/edc.properties";
	private static final String DEFAULT_CONFIG_PROPERTIES_RESOURCE = "edc.properties";

	private Properties properties;
	private List<String> topics;

	public EdcPropertiesConfigurationImplementation() {
		properties = getProperties(null);
		setTopics();
	}

	@Override
	public String getDefaultConfigPropertiesPath() {
		return DEFAULT_CONFIG_PROPERTIES_PATH;
	}

	@Override
	public String getDefaultConfigPropertiesResource() {
		return DEFAULT_CONFIG_PROPERTIES_RESOURCE;
	}

	@Override
	public List<String> getTopics() {
		return topics;
	}

	@Override
	public String getTopicTx(String topic) {
		return properties.getProperty(topic + ".send");
	}

	@Override
	public String getTopicRx(String topic) {
		return properties.getProperty(topic + ".rec");
	}

	@Override
	public String getTopicError() {
		return properties.getProperty("error_topic");
	}

	private void setTopics() {
		topics = new ArrayList<String>();
		for (Object key : properties.keySet()) {
			String keytemp = key.toString();
			keytemp = keytemp.replaceAll(".send", "");
			keytemp = keytemp.replaceAll(".rec", "");
			if (!(keytemp.equals("error_topic"))&&!(topics.contains(keytemp))) {
				topics.add(keytemp);
			}
		}
	}

}
