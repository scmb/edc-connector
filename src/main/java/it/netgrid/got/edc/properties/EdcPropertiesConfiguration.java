package it.netgrid.got.edc.properties;

import java.util.List;

public interface EdcPropertiesConfiguration {
	
	public List<String> getTopics();
	
	public String getTopicTx(String topic);

	public String getTopicRx(String topic);

	public String getTopicError();
	

}
