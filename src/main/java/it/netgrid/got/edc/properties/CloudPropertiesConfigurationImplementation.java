package it.netgrid.got.edc.properties;

import java.util.Properties;

import it.netgrid.got.utils.PropertiesConfigurationTemplate;


public class CloudPropertiesConfigurationImplementation extends PropertiesConfigurationTemplate implements CloudPropertiesConfiguration {

	private static final String DEFAULT_CONFIG_PROPERTIES_PATH = System.getProperty("user.dir")
			+ "/cloud.properties";
	private static final String DEFAULT_CONFIG_PROPERTIES_RESOURCE =  "cloud.properties";

	private Properties properties;
	public CloudPropertiesConfigurationImplementation(){
		this.properties= getProperties(null);
	}
	
	@Override
	public String getAccountName() {
		return properties.getProperty("account_name");
	}

	@Override
	public String getAssetId() {
		return properties.getProperty("asset_id");
	}

	@Override
	public String getBroker() {
		return properties.getProperty("broker");
	}

	@Override
	public String getClientId() {
		return properties.getProperty("client_id");
	}

	@Override
	public String getUser() {
		return properties.getProperty("user");
	}

	@Override
	public String getPassword() {
		return properties.getProperty("password");
	}

	@Override
	public String getMaxPublish() {
		return properties.getProperty("max_publish");
	}

	@Override
	public String getPublishPeriod() {
		return properties.getProperty("publish_period");
	}

	@Override
	public String getDisplayName() {
		return properties.getProperty("display");
	}

	@Override
	public String getModelName() {
		return properties.getProperty("model");
	}

	@Override
	public String getDefaultConfigPropertiesPath() {
		return DEFAULT_CONFIG_PROPERTIES_PATH;
	}

	@Override
	public String getDefaultConfigPropertiesResource() {
		return DEFAULT_CONFIG_PROPERTIES_RESOURCE;
	}

}
