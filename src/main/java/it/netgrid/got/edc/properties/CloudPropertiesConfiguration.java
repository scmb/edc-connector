package it.netgrid.got.edc.properties;

public interface CloudPropertiesConfiguration {

	public static final String HANDLER_NAME="edc-connector";
	
	public String getAccountName();
	
	public String getAssetId();

	public String getBroker();
	
	public String getClientId();
	
	public String getUser();
	
	public String getPassword();
	
	public String getMaxPublish();
	
	public String getPublishPeriod();
	
	public String getDisplayName();
	
	public String getModelName();
}
