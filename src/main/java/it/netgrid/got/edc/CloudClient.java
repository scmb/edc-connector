package it.netgrid.got.edc;

import com.eurotech.cloud.client.EdcClientException;
import com.eurotech.cloud.client.EdcClientFactory;
import com.eurotech.cloud.client.EdcCloudClient;
import com.eurotech.cloud.client.EdcConfiguration;
import com.eurotech.cloud.client.EdcConfigurationFactory;
import com.eurotech.cloud.client.EdcDeviceProfile;
import com.eurotech.cloud.client.EdcDeviceProfileFactory;
import com.eurotech.cloud.message.EdcPayload;
import com.google.inject.Inject;

import it.netgrid.got.edc.properties.CloudPropertiesConfiguration;

public class CloudClient {

	private String ACCOUNT_NAME;
	private String ASSET_ID;
	private String BROKER_URL;
	private String CLIENT_ID;
	private String USERNAME;
	private String PASSWORD;

	private EdcConfigurationFactory confFact;
	private EdcConfiguration conf;
	private EdcDeviceProfileFactory profFactory;
	private EdcDeviceProfile prof;

	private EdcCloudClient edcCloudClient;
	private CloudPropertiesConfiguration cloudConfiguration;
	private CloudTopic handler;

	@Inject
	public CloudClient(CloudPropertiesConfiguration cloudConf, CloudTopic handler) {

		cloudConfiguration = cloudConf;
		ACCOUNT_NAME = cloudConfiguration.getAccountName();
		ASSET_ID = cloudConfiguration.getAssetId();
		BROKER_URL = cloudConfiguration.getBroker();
		CLIENT_ID = cloudConfiguration.getClientId();
		USERNAME = cloudConfiguration.getUser();
		PASSWORD = cloudConfiguration.getPassword();

		confFact = EdcConfigurationFactory.getInstance();
		conf = confFact.newEdcConfiguration(ACCOUNT_NAME, ASSET_ID, BROKER_URL, CLIENT_ID, USERNAME, PASSWORD);
		profFactory = EdcDeviceProfileFactory.getInstance();
		prof = profFactory.newEdcDeviceProfile();

		this.handler = handler;
		prof.setDisplayName(cloudConfiguration.getDisplayName());
		prof.setModelName(cloudConfiguration.getModelName());
	}

	public boolean connect() {
		try {
			edcCloudClient = EdcClientFactory.newInstance(conf, prof, handler);
		} catch (EdcClientException e) {
			e.printStackTrace();
			return false;
		}
		try {
			edcCloudClient.startSession();
		} catch (EdcClientException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean subscribeTo(String topic) {
		try {
			edcCloudClient.subscribe(topic, 1);
		} catch (EdcClientException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean publish(String semanticTopic, EdcPayload payload) {
		try {
			edcCloudClient.publish(semanticTopic, payload, 1, false);
		} catch (EdcClientException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public String getBrokerUrl() {
		return BROKER_URL;
	}

}
