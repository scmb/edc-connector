# EDC Connector #
## Netgrid's [Bauer](https://bitbucket.org/netgrid/bauer) connector for the [Everyware Cloud™](https://www.eurotech.com/en/products/software+services/everyware+device+cloud)##

The edc-connector is a configurable java stand-alone application that allows to bauer-based applications to have direct access to the Everywhare Cloud. 
It works simply by providing a properties file as configuration where the EDC topics are associated to their relative bauer topics.
The connector, by default, publishes over the cloud the messages received from bauer, publishes over bauer's topics the messages received from the EDC and send errors to a configuration defined topic.

## Configuration Example ##

### Connector's configuration ###
You want to listend from the EDC Topic "TOPICS/LISTEN" and send the published messages on the bauer topic "edc_listen", while publishing over cloud's topic "TOPICS/WRITE" the messages sent to the bauer topic "edc_send" your "edc.properties" file will look like this:
```
#!java
TOPICS/LISTEN.send=edc_listen
TOPICS/WRITE.rec=edc_send
error_topic=edc_error_topic
```

### EDC Connection configuration ###
To let the connector access your Everyware account you have to specify these informations in a property file called "cloud.properties" with these properties:

```
#!java
user=username
password=p4ssw0Rd!
account_name=accountName
broker=mqtt://broker-sandbox.everyware-cloud.com:1883
client_id=your application name
asset_id=...
display=...
model=...

```
## Interfacing your app ##
### Message Objects ###
The bauer topics in your application that you want to interface with the connector need to be initialized with a class like this:
```
#!java
public class EDCConnectorMessage {
	private String sender;
	private Map<String, Object> content;

	public EDCConnectorMessage() {
	
        }
        //getters and setters for sender and content
}
```
Except for the error topic object that needs to have a structure like this:
```
#!java
public class EDCConnectorError{
	private String sender;
	private String content;

	public EDCConnectorErrort() {
        
	}
        //getters and setters for sender and contet
}
```
### Setting up Topics ###
```
#!java
Topic<EDCConnectorMessage> publishToEdcTopic=TopicFactory.getTopic("edc_send");
Topic<EDCConnectorMessage> receiveFromEdcTopic=TopicFactory.getTopic("edc_list");
Topic<EDCConnectorError> edcErrorTopic=TopicFactory.getTopic("edc_error");

receiveFromEdcTopic.addHandler(new EventHandler<EDCConnectorMessage>() {
            @Override
            public Class<EDCConnectorMessage> getEventClass() {
                return EDCConnectorMessage.class;
            }
            @Override
            public String getName() {
                return receiveFromEdcTopic.getName();
            }
            @Override
            public boolean handle(EDCConnectorMessage event) {
                // your code
                return true;
            }
        });

edcErrorTopic.addHandler(new EventHandler<EDCConnectorError>() {
            @Override
            public Class<EDCConnectorError> getEventClass() {
                return EDCConnectorError.class;
            }
            @Override
            public String getName() {
                return edcErrorTopic.getName();
            }
            @Override
            public boolean handle(EDCConnectorError event) {
                System.out.println("EDC connector error: "+event.getContent());
                // your code
                return true;
            }
        });

```
### Publish Metrics Over The EDC ###
```
#!java
EDCConnectorMessage message=new EDCConnectorMessage();
Map<String, Object> metrics = new HashMap<String, Object>();
metrics.put("your_metric", "your_value");
metrics.put("your_numeric_metric", 100);

message.setSender("application name or application module");
message.setContent(metrics);

publishToEdcTopic.post(message);

```
## Run command ##
```
#!bash
java -jar edc-connector.jar
```
## How do I get set up? ##

* Include the connector's jar in your runtime directory;
* Create the "edc.properties" and "cloud.properties" configuration files based on examples and be sure the "bauer.properties" file is there too;
* Execute the application;
* Enjoy the fun my dude.

Version: 0.0.3